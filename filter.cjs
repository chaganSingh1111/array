const items = require('./arrays.cjs')

const filter =(items,cb)=>{
    if(!Array.isArray(items)){
        return []
    }
    let filterArr =[]
    for(let idx=0;idx<items.length;idx++){
        let ans = (cb(items[idx],idx,items))
        if(ans === true){
            filterArr.push(items[idx])
        }
            
        
    }
    return filterArr
}

module.exports = filter