const map =(items,cb)=>{
    if(!Array.isArray(items)){
        return []
    }
    let arr = []
    for(let idx=0;idx<items.length;idx++){
        arr.push(cb(items[idx],idx,items))
    }
    return arr
}

module.exports = map