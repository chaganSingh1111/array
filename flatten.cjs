

const flatten = (nestedArray,depth)=>{
    if(depth == undefined){
        depth=1
    }
    if(!Array.isArray(nestedArray)){
        return []
    }
    let arr =[]
    for(let idx=0;idx<nestedArray.length;idx++){
            if(Array.isArray(nestedArray[idx]) && depth>0){
                arr = arr.concat(flatten(nestedArray[idx],depth-1))
            }
            else if(idx in nestedArray){
                arr.push(nestedArray[idx])
            }
    }
    return arr
}

module.exports = flatten