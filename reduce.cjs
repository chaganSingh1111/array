
const reduce =(items,cb,startingValue)=>{
    if(items.length ==0){
        return 
    }
    if(!Array.isArray(items)){
        return undefined
    }
    if(items ==undefined || items ==null){
        return 
    }
    let boolean = true
    if(startingValue ==undefined){
        startingValue=items[0]
        boolean=false
    }

    let ans = startingValue
    for(let idx=0;idx<items.length;idx++){
        if(!boolean && idx ==0){
            idx=1
        }
        ans = cb(ans,items[idx],idx,items)
    
        
    }
    return ans
}

module.exports = reduce