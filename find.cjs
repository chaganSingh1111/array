const items = require('./arrays.cjs')

const find =(items,cb)=>{
    if(!Array.isArray(items)){
        return undefined
    }
    for(let idx=0;idx<items.length;idx++){
        if(cb(items[idx])){
            return items[idx]
        }
    }
    return undefined
}

module.exports = find